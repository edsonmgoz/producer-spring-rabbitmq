package com.rabbit.demo.controller;


import com.rabbit.demo.model.Message;
import com.rabbit.demo.service.MessageService;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.time.LocalDateTime;
@RestController
@CrossOrigin("http://localhost:4200")
public class Producer {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private TopicExchange exchange;

    @Autowired
    private MessageService messageService;

    @PostMapping("/post")
    public String send(@RequestBody Message message) {
        message.setState("enviado");
        message.setFechaRegistro(Timestamp.valueOf(LocalDateTime.now()));
        messageService.saveMessage(message);
        rabbitTemplate.convertAndSend(exchange.getName(), "routing.A", message);
        return "Message sent successfully";
    }
}
