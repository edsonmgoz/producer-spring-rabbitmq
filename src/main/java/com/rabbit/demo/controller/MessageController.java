package com.rabbit.demo.controller;

import com.rabbit.demo.model.Message;
import com.rabbit.demo.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping
    public ResponseEntity<Message> saveMessage(@RequestBody Message message) {
        return ResponseEntity.status(HttpStatus.CREATED).body(messageService.saveMessage(message));
    }

    @GetMapping
    public ResponseEntity<Page<Message>> getAllMessage(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false, defaultValue = "false") Boolean enablePagination
    ) {
        return ResponseEntity.ok(messageService.getAllMessage(page, size, enablePagination));
    }

    @DeleteMapping
    public void deleteMessage(@PathVariable("id") Long id) {
        messageService.deleteMessage(id);
        ResponseEntity.ok(!messageService.existById(id));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Optional<Message>> findMessagebyId(@PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(messageService.findById(id));
    }

    @PutMapping
    public ResponseEntity<Message> editMessage(@RequestBody Message message) {
        return ResponseEntity.status(HttpStatus.CREATED).body(messageService.editMessage(message));
    }
}
